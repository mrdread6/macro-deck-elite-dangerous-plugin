﻿using SuchByte.MacroDeck.Logging;
using System;
using System.IO;
using System.Xml.Serialization;

namespace MrDread.Elite.Utils
{
    public class EliteUtils
    {
        public string bindingPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            @"Frontier Developments\Elite Dangerous\Options\Bindings");
        
        UserBindings _userBindings;

        public UserBindings UserBindings
        {
            get => _userBindings;
        }

        //Check if file directory exists
        public bool CheckForConfig()
        {
            if (!Directory.Exists(bindingPath))
                return false;
            bindingPath = Path.Combine(bindingPath, "Custom.4.0.binds");
            if (!File.Exists(bindingPath))
            {
                bindingPath = bindingPath.Replace("Custom.4.0.binds", "Custom.3.0.binds");
                if (!File.Exists(bindingPath))
                {
                    bindingPath = bindingPath.Replace("Custom.3.0.binds", "Custom.4.1.binds");
                    if(!File.Exists(bindingPath))
                        return false;
                }
            }
            return true;
        }

        public void LoadConfig()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(UserBindings));
            StreamReader reader = new StreamReader(bindingPath);
            _userBindings = (UserBindings)serializer.Deserialize(reader);
            reader.Close();
            MacroDeckLogger.Info(PluginInstance.Main, "Loaded Elite Dangerous config");
        }

    }
}