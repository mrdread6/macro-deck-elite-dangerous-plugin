﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class TargetNextRouteSystem : PluginAction
    {
        public override string Name => "Target Next Route System";
        public override string Description => "Target the next system in the route";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.TargetNextRouteSystem);
        }
    }
}