﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class IncreaseEnginesPower : PluginAction
    {
        public override string Name => "Increase Engines Power";
        public override string Description => "Increase Engines Power";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.IncreaseEnginesPower);
        }
    }
}