﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class ShipSpotLightToggle : PluginAction
    {
        public override string Name => "Ship Spot Light";
        public override string Description => "Turn on/off spot light.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.ShipSpotLightToggle);
        }
    }
}