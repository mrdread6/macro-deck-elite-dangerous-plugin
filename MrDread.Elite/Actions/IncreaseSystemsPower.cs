﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class IncreaseSystemsPower : PluginAction
    {
        public override string Name => "Increase Systems Power";
        public override string Description => "Increase Systems Power";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.IncreaseSystemsPower);
        }
    }
}