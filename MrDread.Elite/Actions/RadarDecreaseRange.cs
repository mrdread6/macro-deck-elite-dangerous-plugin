﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class RadarDecreaseRange : PluginAction
    {
        public override string Name => "Radar Decrease Range";
        public override string Description => "Decrease the range of the radar";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.RadarDecreaseRange);
        }
    }
}