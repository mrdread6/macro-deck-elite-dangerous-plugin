﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class ToggleCargoScoop : PluginAction
    {
        public override string Name => "Cargo Scoop";
        public override string Description => "Deploy or retract cargo scoop.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.ToggleCargoScoop);
        }
    }
}