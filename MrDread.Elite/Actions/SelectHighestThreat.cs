﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class SelectHighestThreat : PluginAction
    {
        public override string Name => "Select Highest Threat";
        public override string Description => "Select Highest Threat.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.SelectHighestThreat);
        }
    }
}