﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class ExplorationFSSQuit : PluginAction
    {
        public override string Name => "Quit FSS Exploration Mode";
        public override string Description => "Quit FSS Exploration Mode";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.ExplorationFSSQuit);
        }
    }
}