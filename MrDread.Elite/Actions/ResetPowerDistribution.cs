﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class ResetPowerDistribution : PluginAction
    {
        public override string Name => "Reset Power Distribution";
        public override string Description => "Reset Power Distribution";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.ResetPowerDistribution);
        }
    }
}