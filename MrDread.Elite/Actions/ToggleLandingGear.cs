﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class ToggleLandingGear : PluginAction
    {
        public override string Name => "Landing Gear";
        public override string Description => "Deploy or retract landing gear.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.LandingGearToggle);
        }
    }
}