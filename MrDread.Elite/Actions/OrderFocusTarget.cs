﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class OrderFocusTarget : PluginAction
    {
        public override string Name => "Order Focus Target";
        public override string Description => "Order Focus Target";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.OrderFocusTarget);
        }
    }
}