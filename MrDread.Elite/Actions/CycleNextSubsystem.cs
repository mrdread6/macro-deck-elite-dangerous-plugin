﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class CycleNextSubsystem : PluginAction
    {
        public override string Name => "Cycle Next Subsystem";
        public override string Description => "Cycle next subsystem";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.CycleNextSubsystem);
        }
    }
}