﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class UseShieldCell : PluginAction
    {
        public override string Name => "Use Shield Cell";
        public override string Description => "Use a Shield Cell";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.UseShieldCell);
        }
    }
}