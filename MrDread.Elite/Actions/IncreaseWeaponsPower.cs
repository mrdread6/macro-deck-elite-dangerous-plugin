﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class IncreaseWeaponsPower : PluginAction
    {
        public override string Name => "Increase Weapons Power";
        public override string Description => "Increase Weapons Power";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.IncreaseWeaponsPower);
        }
    }
}