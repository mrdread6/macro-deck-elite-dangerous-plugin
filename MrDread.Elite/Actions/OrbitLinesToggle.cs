﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class OrbitLinesToggle : PluginAction
    {
        public override string Name => "Orbit Lines";
        public override string Description => "Toggle on and off orbit lines.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.OrbitLinesToggle);
        }
    }
}