﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class SetSpeed75 : PluginAction
    {
        public override string Name => "Set Speed 75";
        public override string Description => "Set Speed 75%.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.SetSpeed75);
        }
    }
}