﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class DeployHeatSink : PluginAction
    {
        public override string Name => "Deploy Heat Sink";
        public override string Description => "Deploy Heat Sink.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.DeployHeatSink);
        }
    }
}