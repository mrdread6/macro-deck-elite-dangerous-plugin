﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class CycleNextHostileTarget : PluginAction
    {
        public override string Name => "Next Hostile Target";
        public override string Description => "Cycle Next Hostile Target.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.CycleNextHostileTarget);
        }
    }
}