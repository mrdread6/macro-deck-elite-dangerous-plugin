﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class CyclePreviousHostileTarget : PluginAction
    {
        public override string Name => "Previous Hostile Target";
        public override string Description => "Cycle Previous Hostile Target.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.CyclePreviousHostileTarget);
        }
    }
}