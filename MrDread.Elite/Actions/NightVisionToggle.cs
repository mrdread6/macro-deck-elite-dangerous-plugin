﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class NightVisionToggle : PluginAction
    {
        public override string Name => "Night Vision";
        public override string Description => "Night Vision Toggle.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.NightVisionToggle);
        }
    }
}