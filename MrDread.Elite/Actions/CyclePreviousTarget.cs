﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class CyclePreviousTarget : PluginAction
    {
        public override string Name => "Previous Target";
        public override string Description => "Cycle Previous Target.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.CyclePreviousTarget);
        }
    }
}