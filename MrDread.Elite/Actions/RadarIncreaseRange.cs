﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class RadarIncreaseRange : PluginAction
    {
        public override string Name => "Radar Increase Range";
        public override string Description => "Increase the range of the radar";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.RadarIncreaseRange);
        }
    }
}