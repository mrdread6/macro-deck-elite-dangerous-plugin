﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class ToggleFlightAssist : PluginAction
    {
        public override string Name => "Toggle FlightAssist";
        public override string Description => "Toggle Flight Assist.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.ToggleFlightAssist);
        }
    }
}