﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class TargetWingman1 : PluginAction
    {
        public override string Name => "Target Wingman 2";
        public override string Description => "Target second wingman.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.TargetWingman1);
        }
    }
}