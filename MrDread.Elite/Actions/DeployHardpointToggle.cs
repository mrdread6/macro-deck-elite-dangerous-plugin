﻿using MrDread.Elite.Utils;
using SuchByte.MacroDeck.ActionButton;
using SuchByte.MacroDeck.Plugins;

namespace MrDread.Elite.Actions
{
    public class DeployHardpointToggle : PluginAction
    {
        public override string Name => "Deploy Hardpoint Toggle";
        public override string Description => "Deploy/Retract Hardpoint.";

        public override void Trigger(string clientId, ActionButton actionButton)
        {
            new KeyboardUtils().TriggerKeyBinding(PluginInstance.EliteBindings.UserBindings.DeployHardpointToggle);
        }
    }
}