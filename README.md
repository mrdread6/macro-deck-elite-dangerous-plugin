# Elite Dangerous plugin for Macro Deck 2
A collection of some useful features for controlling Elite Dangerous

### To use any of the pre-made macros, you must have Elite Dangerous installed on your computer and have the game running in the background. You must also assign any keyboard shortcut in the controls menu to the actions you want to use in the game.

### This is a plugin for [Macro Deck 2](https://github.com/SuchByte/Macro-Deck), it does NOT function as a standalone app
<img height="64px" src="https://macrodeck.org/images/macro_deck_2_official_plugin.png" />

# Installation
 1. Make sure that the Elite Dangerous key bindings configuration is in `C:\Users\<USER_NAME>\AppData\Local\Frontier Developments\Elite Dangerous\Options\Bindings`
2. Move `MrDread.Elite` folder to `C:\Users\<USER_NAME>\AppData\Roaming\Macro Deck\plugins`
3. Assign a keyboard shortcut to the selected function as an alternative control, such as: **L SHIFT + A**
<img src="https://gitlab.com/mrdread6/macro-deck-elite-dangerous-plugin/-/raw/main/Images/eg.png" />
4. Configure button In Macro Deck
<img src="https://gitlab.com/mrdread6/macro-deck-elite-dangerous-plugin/-/raw/main/Images/md_button.png" />

#### Recommended icons: https://github.com/Ordo-Corona-Stellarum/streamdeck-elite-icons
# Third party licenses
This plugin uses some awesome 3rd party libraries:
- [Newtonsoft.Json (MIT license)](https://www.newtonsoft.com/json)
- [H.InputSimulator (MS-PL)](https://github.com/HavenDV/H.InputSimulator)
- [streamdeck-elite (MIT License)](https://github.com/mhwlng/streamdeck-elite)

## Known issue: 
- Failed to check for updates
